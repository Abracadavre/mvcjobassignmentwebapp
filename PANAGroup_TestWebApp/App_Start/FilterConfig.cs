﻿using System.Web;
using System.Web.Mvc;

namespace PANAGroup_TestWebApp
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters( GlobalFilterCollection filters )
		{
			filters.Add( new HandleErrorAttribute() );
		}
	}
}