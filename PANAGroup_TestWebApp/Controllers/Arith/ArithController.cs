﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PANAGroup_TestWebApp.Models;

namespace PANAGroup_TestWebApp.Controllers
{
	public class ArithController : Controller
	{
		public ActionResult Index( ArithModel model )
		{
			if ( ModelState.IsValid )
			{
				ViewBag.Sum = ( model.M + model.N ).ToString();
				ViewBag.Multi = ( model.M * model.N ).ToString();
				if ( model.N != 0 )
				{
					ViewBag.Divide = ( model.M / model.N ).ToString();
					ViewBag.Rem = ( model.M % model.N ).ToString();
				}
				else
				{
					ViewBag.Divide = "?";
					ViewBag.Rem = "?";
				}
			}
			return View();
		}
	}
}
