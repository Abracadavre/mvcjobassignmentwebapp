﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PANAGroup_TestWebApp.Models;
using PANAGroup_TestWebApp.Utility;

namespace PANAGroup_TestWebApp.Controllers
{
	public class ODataController : Controller
	{
		//
		// GET: /OData/

		public ActionResult WebGrid( int page = 1, int rowsPerPage = 10, string sort = "ProductID", string sortDir = "Ascending" )
		{
			Uri uri = new Uri( "http://services.odata.org/Northwind/Northwind.svc/" );
			var container = new NorthwindODataService.NorthwindEntities( uri );
			var a = container.Products.ToList();

			int totalRecords;

			var products = new ProductsModel().GetProductsFromOData( out totalRecords, pageSize: rowsPerPage, pageIndex: page - 1, sort: sort, sortOrder: Sorting.GetSortDirection( sortDir ) );

			var model = new PagingODataProductsModel
			{
				PageSize = rowsPerPage,
				PageNumber = page,
				Products = products,
				TotalRows = totalRecords
			};


			return View( model );
		}
	}
}
