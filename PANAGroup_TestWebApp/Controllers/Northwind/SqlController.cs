﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PANAGroup_TestWebApp.Models;
using PANAGroup_TestWebApp.Utility;

namespace PANAGroup_TestWebApp.Controllers
{
	public class SqlController : Controller
	{
		public ActionResult WebGrid( int page = 1, int rowsPerPage = 10, string sort = "ProductID", string sortDir = "Ascending" )
		{
			int totalRecords;
			var products = new ProductsModel().GetProductsFromSQL( out totalRecords, pageSize: rowsPerPage, pageIndex: page - 1, sort: sort, sortOrder: Sorting.GetSortDirection( sortDir ) );

			var model = new PagingSQLProductsModel
			{
				PageSize = rowsPerPage,
				PageNumber = page,
				Products = products,
				TotalRows = totalRecords
			};

			return View( model );
		}

	}
}
