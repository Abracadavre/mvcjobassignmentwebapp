﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PANAGroup_TestWebApp.Models
{
	public class PagingODataProductsModel
	{
		public int PageNumber { get; set; }

		public IEnumerable<PANAGroup_TestWebApp.NorthwindODataService.Product> Products { get; set; }

		public int TotalRows { get; set; }

		public int PageSize { get; set; }
	}
}