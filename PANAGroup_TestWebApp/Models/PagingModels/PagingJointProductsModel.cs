﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PANAGroup_TestWebApp.Models
{
	public class PagingJointProductsModel
	{
		public int PageNumber { get; set; }

		public IEnumerable<JointProductModel> Products { get; set; }

		public int TotalRows { get; set; }

		public int PageSize { get; set; }
	}
}