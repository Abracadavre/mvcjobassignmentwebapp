﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PANAGroup_TestWebApp.Models
{
	public class PagingSQLProductsModel
	{
		public int PageNumber { get; set; }

		public IEnumerable<Product> Products { get; set; }

		public int TotalRows { get; set; }

		public int PageSize { get; set; }
	}
}