﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Xml.Linq;
using PANAGroup_TestWebApp.Controllers;
using PANAGroup_TestWebApp.Utility;

namespace PANAGroup_TestWebApp.Models
{
	public class ProductsModel
	{
		#region Private

		private readonly IDictionary<string, Func<IQueryable<Product>, bool, IOrderedQueryable<Product>>> _productOrderings = new Dictionary<string, Func<IQueryable<Product>, bool, IOrderedQueryable<Product>>>
                                    {
                                        {"ProductID", CreateOrderingFunc<Product, int>(p=>p.ProductID)},
                                        {"ProductName", CreateOrderingFunc<Product, string>(p=>p.ProductName)},
										{"Category.CategoryName", CreateOrderingFunc<Product, string>(p=>p.Category.CategoryName)},
										{"Supplier.CompanyName", CreateOrderingFunc<Product, string>(p=>p.Supplier.CompanyName)},
										{"Supplier.Country", CreateOrderingFunc<Product, string>(p=>p.Supplier.Country)}
                                    };

		private readonly IDictionary<string, Func<IQueryable<JointProductModel>, bool, IOrderedQueryable<JointProductModel>>> _productOrderingsJoint = new Dictionary<string, Func<IQueryable<JointProductModel>, bool, IOrderedQueryable<JointProductModel>>>
                                    {
                                        {"ProductID", CreateOrderingFunc<JointProductModel, int>(p=>p.ProductID)},
                                        {"ProductName", CreateOrderingFunc<JointProductModel, string>(p=>p.ProductName)},
										{"CategoryName", CreateOrderingFunc<JointProductModel, string>(p=>p.CategoryName)},
										{"CompanyName", CreateOrderingFunc<JointProductModel, string>(p=>p.CompanyName)},
										{"Country", CreateOrderingFunc<JointProductModel, string>(p=>p.Country)}
                                    };

		private readonly IDictionary<string, Func<IQueryable<PANAGroup_TestWebApp.NorthwindODataService.Product>, bool, IOrderedQueryable<PANAGroup_TestWebApp.NorthwindODataService.Product>>> _productOrderingsOData = new Dictionary<string, Func<IQueryable<PANAGroup_TestWebApp.NorthwindODataService.Product>, bool, IOrderedQueryable<PANAGroup_TestWebApp.NorthwindODataService.Product>>>
                                    {
                                        {"ProductID", CreateOrderingFunc<PANAGroup_TestWebApp.NorthwindODataService.Product, int>(p=>p.ProductID)},
                                        {"ProductName", CreateOrderingFunc<PANAGroup_TestWebApp.NorthwindODataService.Product, string>(p=>p.ProductName)},
										{"Category.CategoryName", CreateOrderingFunc<PANAGroup_TestWebApp.NorthwindODataService.Product, string>(p=>p.Category.CategoryName)},
										{"Supplier.CompanyName", CreateOrderingFunc<PANAGroup_TestWebApp.NorthwindODataService.Product, string>(p=>p.Supplier.CompanyName)},
										{"Supplier.Country", CreateOrderingFunc<PANAGroup_TestWebApp.NorthwindODataService.Product, string>(p=>p.Supplier.Country)}
                                    };

		private static Func<IQueryable<T>, bool, IOrderedQueryable<T>> CreateOrderingFunc<T, TKey>( Expression<Func<T, TKey>> keySelector )
		{
			return
				( source, ascending ) =>
				ascending
					? source.OrderBy( keySelector )
					: source.OrderByDescending( keySelector );
		}

		#endregion Private

		#region Public API

		/// <summary>
		/// Get products from object without paging
		/// </summary>
		public IEnumerable<JointProductModel> GetProductsWithoutPaging()
		{
			var seed = new Seed();

			var products = seed.ProductsList;
			var categories = seed.CategoriesList;
			var suppliers = seed.SuppliersList;

			var result = products.Select( p => new JointProductModel
			{
				ProductID = p.ProductID,
				ProductName = p.ProductName,
				CategoryName = categories.FirstOrDefault( c => c.CategoryID == p.CategoryID ).CategoryName,
				CompanyName = suppliers.FirstOrDefault( s => s.SupplierID == p.SupplierID ).CompanyName,
				Country = suppliers.FirstOrDefault( s => s.SupplierID == p.SupplierID ).Country
			} );
			return result;

		}

		/// <summary>
		/// Get products from object with paging
		/// </summary>
		public IEnumerable<JointProductModel> GetProductsFromObject( out int totalRecords, int pageSize, int pageIndex, string sort, SortDirection sortOrder )
		{
			var seed = new Seed();

			var products = seed.ProductsList;
			var categories = seed.CategoriesList;
			var suppliers = seed.SuppliersList;

			IQueryable<JointProductModel> result = products.Select( p => new JointProductModel
			{
				ProductID = p.ProductID,
				ProductName = p.ProductName,
				CategoryName = categories.FirstOrDefault( c => c.CategoryID == p.CategoryID ).CategoryName,
				CompanyName = suppliers.FirstOrDefault( s => s.SupplierID == p.SupplierID ).CompanyName,
				Country = suppliers.FirstOrDefault( s => s.SupplierID == p.SupplierID ).Country
			} ).AsQueryable();

			totalRecords = result.Count();

			Func<IQueryable<JointProductModel>, bool, IOrderedQueryable<JointProductModel>> applyOrdering = _productOrderingsJoint[sort];
			result = applyOrdering( result, sortOrder == SortDirection.Ascending );

			if ( pageSize > 0 && pageIndex >= 0 )
			{
				result = result.Skip( pageIndex * pageSize ).Take( pageSize );
			}

			return result.ToList();
		}

		/// <summary>
		/// Get products from SQL database with paging
		/// </summary>
		public IEnumerable<Product> GetProductsFromSQL( out int totalRecords, int pageSize, int pageIndex, string sort, SortDirection sortOrder )
		{
			using ( var db = new Northwind( System.Configuration.ConfigurationManager.ConnectionStrings["North"].ConnectionString ) )
			{
				DataLoadOptions options = new DataLoadOptions();

				options.LoadWith<Product>( item => item.Supplier );
				options.LoadWith<Product>( item => item.Category );

				db.LoadOptions = options;

				IQueryable<Product> products = db.Products;

				totalRecords = products.Count();

				Func<IQueryable<Product>, bool, IOrderedQueryable<Product>> applyOrdering = _productOrderings[sort];
				products = applyOrdering( products, sortOrder == SortDirection.Ascending );

				if ( pageSize > 0 && pageIndex >= 0 )
				{
					products = products.Skip( pageIndex * pageSize ).Take( pageSize );
				}

				return products.ToList();
			}
		}

		/// <summary>
		/// Get products from OData Service with paging
		/// </summary>
		public IEnumerable<PANAGroup_TestWebApp.NorthwindODataService.Product> GetProductsFromOData( out int totalRecords, int pageSize, int pageIndex, string sort, SortDirection sortOrder )
		{
			Uri uri = new Uri( "http://services.odata.org/Northwind/Northwind.svc/" );
			var entities = new NorthwindODataService.NorthwindEntities( uri );

			IQueryable<PANAGroup_TestWebApp.NorthwindODataService.Product> products = entities.Products.Expand( p => p.Supplier ).Expand( p => p.Category );

			totalRecords = products.Count();

			Func<IQueryable<PANAGroup_TestWebApp.NorthwindODataService.Product>, bool, IOrderedQueryable<PANAGroup_TestWebApp.NorthwindODataService.Product>> applyOrdering = _productOrderingsOData[sort];
			products = applyOrdering( products, sortOrder == SortDirection.Ascending );

			if ( pageSize > 0 && pageIndex >= 0 )
			{
				products = products.Skip( pageIndex * pageSize ).Take( pageSize );
			}

			return products.ToList();
		}

		/// <summary>
		/// Get products from XML with paging
		/// </summary>
		public IEnumerable<JointProductModel> GetProductsFromXML( out int totalRecords, int pageSize, int pageIndex, string sort, SortDirection sortOrder )
		{
			string path = System.Web.HttpContext.Current.Server.MapPath( @"~\App_Data" );
			var DocumentProducts = XElement.Load( path + @"\XProducts.xml" );
			var DocumentCategories = XElement.Load( path + @"\XCategories.xml" );
			var DocumentSuppliers = XElement.Load( path + @"\XSuppliers.xml" );

			IQueryable<JointProductModel> products = DocumentProducts.Descendants( "Product" ).Select( p =>
																						new JointProductModel
																						{
																							ProductID = Convert.ToInt32( p.Descendants( "ProductID" ).First().Value ),
																							ProductName = p.Descendants( "ProductName" ).First().Value,
																							CategoryName = DocumentCategories.Descendants( "Category" ).FirstOrDefault( c => c.Descendants( "CategoryID" ).First().Value == p.Descendants( "CategoryID" ).First().Value ).Descendants( "CategoryName" ).First().Value,
																							CompanyName = DocumentSuppliers.Descendants( "Supplier" ).FirstOrDefault( c => c.Descendants( "SupplierID" ).First().Value == p.Descendants( "SupplierID" ).First().Value ).Descendants( "CompanyName" ).First().Value,
																							Country = DocumentSuppliers.Descendants( "Supplier" ).FirstOrDefault( c => c.Descendants( "SupplierID" ).First().Value == p.Descendants( "SupplierID" ).First().Value ).Descendants( "Country" ).First().Value
																						} ).AsQueryable();

			totalRecords = products.Count();

			Func<IQueryable<JointProductModel>, bool, IOrderedQueryable<JointProductModel>> applyOrdering = _productOrderingsJoint[sort];
			products = applyOrdering( products, sortOrder == SortDirection.Ascending );

			if ( pageSize > 0 && pageIndex >= 0 )
			{
				products = products.Skip( pageIndex * pageSize ).Take( pageSize );
			}

			return products.ToList();
		}

		#endregion Public API
	}
}