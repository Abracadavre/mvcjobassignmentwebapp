﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PANAGroup_TestWebApp.Utility
{
	public static class Sorting
	{
		public static SortDirection GetSortDirection( string sortDirection )
		{
			if ( sortDirection != null )
			{
				if ( sortDirection.Equals( "DESC", StringComparison.OrdinalIgnoreCase ) ||
					sortDirection.Equals( "DESCENDING", StringComparison.OrdinalIgnoreCase ) )
				{
					return SortDirection.Descending;
				}
			}
			return SortDirection.Ascending;
		}
	}
}